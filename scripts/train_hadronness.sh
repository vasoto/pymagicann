#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PYTHONPATH=$DIR/../:$PYTHONPATH
source $DIR/common.sh
python3 $DIR/../bin/train_classification.py -c $DIR/../data/20180731/train_hadronness.rc > hadronness.log 2>&1

