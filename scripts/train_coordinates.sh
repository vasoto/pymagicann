#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PYTHONPATH=$DIR/../:$PYTHONPATH
source $DIR/common.sh
python3 $DIR/../bin/train.py -c $DIR/../data/20180731/train_coords.rc > coordinates.log 2>&1

