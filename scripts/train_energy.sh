#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PYTHONPATH=$DIR/../:$PYTHONPATH
source $DIR/common.sh
python3 $DIR/../bin/train.py -c $DIR/../data/20180731/energy_stereo_log10.rc > energy_stereo_log10.log 2>&1

