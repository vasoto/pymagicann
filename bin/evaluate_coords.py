'''
Created on Aug 16, 2018

@author: vasot
'''
import argparse
import glob
import logging
import pickle
import sys
import os

import numpy as np
import ROOT as R
import pandas
import tqdm

from pymarsann.config import TrainingConfig
from pymarsann.models.energy_simple import create_model
from pymarsann.data.preprocess._preprocess import DataPreprocessor
from pymarsann.utils.sentry import init_sentry


init_sentry()


try:
    import coloredlogs
    coloredlogs.install(level='DEBUG')
except Exception as _: #ModuleNotFoundError as _:
    pass


pandas.set_option('display.max_rows', 500)
pandas.set_option('display.max_columns', 500)
pandas.set_option('display.width', 2000)


logger = logging.getLogger('train')
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s: [%(module)s] %(message)s")



def parse_arguments(args):
    parser = argparse.ArgumentParser(description="Evaluate data using given model.")
    parser.add_argument("-w",
                        "--weights",
                        dest='model_weights_file',
                        required=True,
                        help="Model")
    parser.add_argument("-c",
                        "--config",
                        dest="config_file",
                        required=True,
                        help="Configuration file")
    parser.add_argument("-i",
                        "--inputs",
                        dest='input_files',
                        required=True,
                        help="Input ROOT file(s)")
    parser.add_argument("-o",
                        "--outdir",
                        dest='output_dir',
                        default=".",
                        help="Output directory")
    parser.add_argument("-t",
                        "--tree",
                        dest='tree_name',
                        default="Events",
                        help="Tree name")
    parser.add_argument("-s",
                        "--scaler",
                        dest='scaler_file',
                        required=True,
                        help="Data scaler dump file name")
    return parser.parse_args(args)


class RootEvaluator(object):
    def __init__(self, config, model, scaler, tree_name):
        self.config = config
        self.model = model
        self.tree_name = tree_name
        self.preprocessor = DataPreprocessor(self.config,
                                             scaler=scaler)
        self._input_root_file = None
        self._output_root_file = None
        self._input_tree = None
        self._output_tree = None
        self._energy = None
        self._energy_raw = None
        
    
    def _reset(self):
        self._root_file = None
        self._input_tree = None
        self._output_tree = None
        self._energy = None
    
    @classmethod
    def load_from_args(cls, args):
        config = TrainingConfig(args.config_file).load()
        model = create_model(config) #TODO - dump the whole model and its weights
        model.load_weights(args.model_weights_file)
        scaler = pickle.load(open(args.scaler_file, 'r'))
        return cls(config=config,
                   model=model,
                   scaler=scaler,
                   tree_name=args.tree_name)

    def _output_file_name(self, input_file_name, output_dir):
        _, file_name = os.path.split(input_file_name)
        return os.path.join(output_dir, file_name.replace('_S_', '_Q_'))

    def _initialize_input(self, input_file):
        # Open ROOT input file
        self._input_root_file = R.TFile(input_file)
        # Load tree
        self._input_tree = self._input_root_file.Get(self.tree_name)
        # Enable branches
        print(self._input_tree, self.tree_name, self._input_root_file)
        self._input_tree.SetBranchStatus("*", 0) # Disable all branches
        for branch in self.config.branches:
            self._input_tree.SetBranchStatus(branch, 1)

    def _load_data(self, input_file):
        self._initialize_input(input_file)
        num_events = self._input_tree.GetEntries()
#         num_events = 100
        # Create empty numpy array (faster than zeros)
        data = np.empty((num_events,
                         len(self.config.branches)))
        logger.info("Reading {0} events...".format(num_events))
        # Read events and fill the selected branches into the data array
        for event_num in tqdm.tqdm(range(num_events)):
            self._input_tree.GetEntry(event_num)
            chunk = np.array([self._input_tree.GetLeaf(branch).GetValue()
                     for branch in self.config.branches])
            data[event_num, :] = chunk
        # Create pandas DataFrame from the data
        result = pandas.DataFrame(data, columns=self.config.branches)
        return result

    def _preprocess(self, data):
        return self.preprocessor.fit(data)
    
    def _predict(self, X):
        return self.model.predict(X)
    
    def _postprocess(self, predicted):
        # Inverse normalization
        result = self.preprocessor.scaler.inverse(predicted,
                                                  features=[br.replace('.',
                                                                       '_')
                                                            for br in self.config.targets])
        return result
    
    def _write_output(self, output_file, result, target):
        # Indexes of the original events
        indexes = self.preprocessor.index_after_filter

        self._output_root_file = R.TFile(output_file, "RECREATE")
        self._output_tree = self._input_tree.CloneTree(0)
        self._position = R.MSrcPosCam("MSrcPosCam_DL",
                                      "Position estimated by Neural Net")

        self._output_tree.Branch("MSrcPosCam_DL.",
                                 "MSrcPosCam",
                                 self._position)
        logger.info("Writing output ROOT file ...")
        y_inv = result # 10 ** result # reconstruct prediction
        y_test = self._postprocess(target) # inverse normalization of target
        err = np.empty_like(result)
        err1 = np.empty_like(result)

        for i, event_index in tqdm.tqdm(enumerate(indexes),
                                        total=len(indexes)):
            self._input_tree.GetEntry(event_index)
            self._position.SetXY(float(y_inv[i][0]),
                                 float(y_inv[i][1]))
            err = (y_test[i] - y_inv[i]) / y_test[i]
            self._output_tree.Fill()
        self._output_tree.Write()
        self._output_root_file.Close()

        logger.debug("Non-inversed data mean error: min={0} mean={1} std={3} max={2}".format(err.min(),
                                                                   err.mean(),
                                                                   err.max(),
                                                                   err.std()))
        logger.debug("Inversed data mean error: min={0} mean={1} std={3} max={2}".format(err1.min(),
                                                                   err1.mean(),
                                                                   err1.max(),
                                                                   err1.std()))
    
    def _evaluate_file(self, input_file, output_dir):
        output_file = self._output_file_name(input_file,
                                             output_dir)
        self._reset()
        data = self._load_data(input_file)
        # X and y are normalised inputs and targets
        X, target = self._preprocess(data)
        # predicted is the normalized output of the neural net
        predicted = self._predict(X)
        logger.info("Y_norm: [{0} - {1}] ({2}; {3})".format(target.min(),
                                              target.max(),
                                              target.mean(),
                                              target.std()))
        logger.info("Y'_pred: [{0} - {1}] ({2}; {3})".format(predicted.min(),
                                              predicted.max(),
                                              predicted.mean(),
                                              predicted.std()))

        result = self._postprocess(predicted)
        self._write_output(output_file, result, target)
    
    def execute(self, input_files, output_dir):
        for input_file in glob.glob(input_files):
            logger.info("Processing file {input_file}".format(input_file=
                                                              input_file))
            self._evaluate_file(input_file, output_dir)


def set_root_env():
    mars_path = os.environ['MARSSYS']
    return R.gROOT.Macro("{0}/macros/rootlogon.C".format(mars_path))


def main(args=sys.argv[1:]):
    set_root_env()
    args = parse_arguments(args)
    evaluator = RootEvaluator.load_from_args(args)
    evaluator.execute(args.input_files, args.output_dir)


if __name__ == '__main__':
    DATA_DIR = r'../../data/20180731/'

    root_file_name = '{data_dir}/GA_*75*_1.root'.format(data_dir=DATA_DIR)
    config_file_name =  '{data_dir}/energy_stereo.rc'.format(data_dir=DATA_DIR)
    model_file_name = "../models/model.hdf5"
    scaler_file_name = "../models/scaler.pickle"
    
    DATA_DIR = r'../../data/ST0307/'
   
    root_file_name = '{data_dir}/Test/GA_za*_8_S_w0_2.root'.format(data_dir=DATA_DIR)
    config_file_name =  '{data_dir}/energy_stereo_train.rc'.format(data_dir=DATA_DIR)
    model_file_name = "../models/model_energy_raw.hdf5"
    scaler_file_name = "../models/scaler_energy_raw.pickle"
    
    DATA_DIR = r'./data'
   
    root_file_name = '{data_dir}/ST0307/Test/GA_za*_8_S_w0_2.root'.format(data_dir=DATA_DIR)
    config_file_name =  '{data_dir}/20180731/energy_coords.rc'.format(data_dir=DATA_DIR)
    model_file_name = "coords_model.hdf5"
    scaler_file_name = "coords_scaler.pickle"

    main(['-c', config_file_name,
          '-i', root_file_name,
          '-w', model_file_name,
          '-s', scaler_file_name,
          '-t', 'Events'
          ])
