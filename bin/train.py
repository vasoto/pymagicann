from pymarsann.training.regression import load_data, get_preprocessor
from pymarsann.training.main import main
from pymarsann.config import TrainingConfig
from pymarsann.utils.sentry import init_sentry


init_sentry()


if __name__ == '__main__':
    main(load_data,
         get_preprocessor,
         TrainingConfig)
