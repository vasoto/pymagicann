from pymarsann.training.classification import load_data, get_preprocessor
from pymarsann.training.main import main
from pymarsann.config import TrainingConfigHadronness
from pymarsann.utils.sentry import init_sentry


init_sentry()


if __name__ == '__main__':
    main(load_data_fn=load_data,
         preprocessor_fn=get_preprocessor,
         cfg_class=TrainingConfigHadronness)
