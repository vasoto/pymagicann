from pymarsann.training.regression import load_data
from pymarsann.training.test import main
from pymarsann.config import TrainingConfig

if __name__ == '__main__':
    main(load_data, TrainingConfig)
