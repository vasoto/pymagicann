from pymarsann.models.energy_simple import create_model
from pymarsann.models.io import dump_model
from pymarsann.config import TrainingConfig
import argparse
import sys


def parse_arguments(args=sys.argv[1:]):
    ''' Parse command line arguments
    '''
    parser = argparse.ArgumentParser(description="Train a model.")
    parser.add_argument("-c",
                        "--config",
                        dest="config_file",
                        required=True,
                        help="Input configuration file")
    parser.add_argument("-w",
                        "--weights",
                        dest="weights_file",
                        default=None,
                        required=False,
                        help="Input weights file")
    parser.add_argument("-m",
                        "--model",
                        dest="model_file",
                        required=True,
                        help="Output model file")
    return parser.parse_args(args)


if __name__ == '__main__':
    args = parse_arguments()
    config = TrainingConfig(args.config_file).load()
    model = create_model(config)
    weights_file = args.weights_file
    if weights_file is None:
        weights_file = "{0}.hdf5".format(config.model_output_prefix)
    model.load_weights(weights_file)
    dump_model(model, args.model_file)
