ARG cuda_version=9.0
ARG cudnn_version=7
ARG root_version="5.34.36"
ARG mars_version="V2-18-0"

FROM nvidia/cuda:${cuda_version}-cudnn${cudnn_version}-devel

# Install system packages
RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      cmake \
      davix-dev \
      dcap-dev \
      fonts-freefont-ttf \
      g++ \
      gcc \
      gfal2 \
      gfortran \
      git \
      libafterimage-dev \
      libavahi-compat-libdnssd-dev \
      libcfitsio-dev \
      libfftw3-dev \
      libfreetype6-dev \
      libftgl-dev \
      libgfal2-dev \
      libgif-dev \
      libgl2ps-dev \
      libglew-dev \
      libglu-dev \
      libgraphviz-dev \
      libgsl-dev \
      libjemalloc-dev \
      libjpeg-dev \
      libkrb5-dev \
      libldap2-dev \
      liblz4-dev \
      liblzma-dev \
      libmysqlclient-dev \
      libpcre++-dev \
      libpng-dev\ 
      libpq-dev \
      libpythia8-dev \
      libqt4-dev \
      libsqlite3-dev \
      libssl-dev \
      libtbb-dev \
      libtiff-dev \
      libx11-dev \
      libxext-dev \
      libxft-dev \
      libxml2-dev \
      libxpm-dev \
      libz-dev \
      locales \
      lsb-release \
      make \
      python3-dev \
      python3-numpy \
      python3-pip \
      r-base \
      r-cran-rcpp \
      r-cran-rinside \
      srm-ifce-dev \
      unixodbc-dev \
      bzip2 \
      graphviz \
      libgl1-mesa-glx \
      libhdf5-dev \
      openmpi-bin \
      emacs-nox \
      less \
      wget && \
      localedef -i en_US -f UTF-8 en_US.UTF-8 && \
      rm -rf /var/lib/apt/lists/*

ENV ROOTSYS         "/opt/root"
ENV PATH            "$ROOTSYS/bin:$PATH"
ENV LD_LIBRARY_PATH "$ROOTSYS/lib:$LD_LIBRARY_PATH"
ENV PYTHONPATH      "$ROOTSYS/lib:$PYTHONPATH"

### Anaconda

# Install conda
#ENV CONDA_DIR /opt/conda
#ENV PATH $CONDA_DIR/bin:$PATH

#RUN wget --quiet --no-check-certificate https://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh && \
#    echo "c59b3dd3cad550ac7596e0d599b91e75d88826db132e4146030ef471bb434e9a *Miniconda3-4.2.12-Linux-x86_64.sh" | sha256sum -c - && \
#        /bin/bash /Miniconda3-4.2.12-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
#            rm Miniconda3-4.2.12-Linux-x86_64.sh && \
#                echo export PATH=$CONDA_DIR/bin:'$PATH' > /etc/profile.d/conda.sh

# Install Python packages and keras
#ENV NB_USER keras
#ENV NB_UID 1000

#RUN useradd -m -s /bin/bash -N -u $NB_UID $NB_USER && \
#    chown $NB_USER $CONDA_DIR -R && \
#        mkdir -p /src && \
#            chown $NB_USER /src

#USER $NB_USER

#ARG python_version=3.6
#RUN conda install -y python=${python_version} && \
#RUN pip3 install --upgrade pip && \
RUN pip3 install -U setuptools
RUN pip3 install \
                sklearn_pandas \
                tensorflow-gpu
RUN pip3 install \
                h5py \
                matplotlib \
                nose \
                pydot \
                keras \
                pyyaml \
                scikit-learn \
                notebook \
                Pillow \
                pandas \
                six \
                theano \
                uproot \
                numpy
### ROOT
USER root

ARG root_version="5.34.36"
# Download Root library
RUN wget "https://root.cern.ch/download/root_v${root_version}.source.tar.gz" -O /var/tmp/root.tar.gz
RUN cd /var/tmp && \
    tar xzf root.tar.gz && ls -alh . && \
    cd root && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=$ROOTSYS \
		  -DCMAKE_INSTALL_LOCAL_ONLY=YES \
          -Dccache=ON \
          -Dchirp=OFF \
          -Dfail-on-missing=ON \
          -Dgnuinstall=ON -Drpath=ON \
          -Dbuiltin_zlib=ON \
          -Dfortran=ON \
	  -Dminuit2=ON \
          -Dx11=ON \
          -Drfio=OFF \
          -Dsapdb=OFF \
          -Dsrp=OFF \
          -Dcastor=OFF \
          -Dpgsql=OFF \
          -Doracle=OFF \
	  -Dssl=OFF \
	  -Dodbc=OFF \
	  -Dsqlite=ON \
	  -Dpythia6=OFF \
          -Dpythia8=OFF \
          -Dgfal=OFF \
	  -Ddcache=OFF \
	  -Dldap=OFF \
	  -Dxrootd=OFF ..
RUN cd /var/tmp/root/build && make -s
RUN cd /var/tmp/root/build && make -s install
RUN mkdir /Work


#ADD theanorc /home/keras/.theanorc
ARG mars_version="V2-18-0"
USER root
ENV PYTHONPATH '/src/:$PYTHONPATH'
ENV MARSSYS "/opt/Mars_${mars_version}"
ENV OSTYPE "linux-gnu"
COPY Mars_${mars_version}.tgz /tmp/Mars.tgz
RUN ls -alhtr /tmp
#RUN mkdir /opt && \
RUN cd /tmp && tar xzf ./Mars.tgz -C /opt && ls -alhtr /tmp
RUN ls -alh /usr/local
RUN echo $ROOTSYS && ls -alhtr $ROOTSYS
RUN echo $LD_LIBRARY_PATH && echo $PATH
RUN echo $MARSSYS
RUN cd $MARSSYS && cat Makefile.rules
RUN cd $MARSSYS && sed -r -i 's/\-c \$\(INCLUDES\)/\-c \-p \$\(INCLUDES\)/g' Makefile.rules
RUN cd $MARSSYS && cat Makefile.rules && make -s
ENV PATH "$MARSSYS:$PATH"
ENV LD_LIBRARY_PATH "$MARSSYS:$LD_LIBRARY_PATH"

COPY requirements-gpu.txt /tmp
RUN cd /tmp && pip3 install -r requirements-gpu.txt
RUN pip3 install -U numpy
WORKDIR /Work

EXPOSE 8888
ENV PYTHONPATH "/Work/:$ROOTSYS/lib/root"

CMD jupyter notebook --port=8888 --ip=0.0.0.0
#ENTRYPOINT ["/bin/bash"]
