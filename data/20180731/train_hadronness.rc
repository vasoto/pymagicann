#
#  hadronness.rc
#
TrainHadronness.Type: Hadronness
TrainHadronness.Filter0: MHillas_1.fSize > 10
TrainHadronness.Filter1: MHillas_2.fSize > 10
TrainHadronness.Filter2: (MHillas_1.fSize > 30)||(MHillas_2.fSize > 30)
TrainHadronness.Filter3: (MHillas_1.fSize > 30)&&(MHillas_2.fSize > 30)
# Number of islands cut
TrainHadronness.Filter4: (MImagePar_1.fNumIslands < 6)&&(MImagePar_2.fNumIslands < 6)
# Number of core pixels cut
TrainHadronness.Filter5: (MNewImagePar_1.fNumCorePixels > 2)&&(MNewImagePar_2.fNumCorePixels > 2)
# Leakage1 cut
TrainHadronness.Filter6: ( MNewImagePar_1.fLeakage1 < 1.55 )&&(MNewImagePar_2.fLeakage1 < 1.55 )
# Cherenkov Radius cut
TrainHadronness.Filter7: ( MStereoPar.fCherenkovRadius > 0. )
# Cherenkov Density cut
TrainHadronness.Filter8: ( MStereoPar.fCherenkovDensity > 0. )
# Theta2 cut
TrainHadronness.Filter9: ( MStereoPar.fTheta2<10.5 )
# MStereoPar valid Cut
TrainHadronness.Filter10: ( MStereoPar.fValid > 0.5 )

TrainHadronness.ProtonTrainFiles: ../classification/hadron/*.root
TrainHadronness.GammaTrainFiles: ../classification/gamma/*.root

# ANN variables
TrainHadronness.Layers: 22,16,8,4,1
TrainHadronness.Epochs: 11000
TrainHadronness.Normalizer.Min: 0.1
TrainHadronness.Normalizer.Max: 0.9

# Possible options for activation function are: tanh, sigmoid, linear, relu, elu, selu, leakyrelu, etc.
# For more info about the available activations check https://keras.io/activations/
TrainHadronness.HiddenActivation: tanh
TrainHadronness.OutputActivation: sigmoid
TrainHadronness.KernelNormalizer: glorot_uniform
#lecun_normal
TrainHadronness.ModelOutputFile: hadronness.model
TrainHadronness.Loss: binary_crossentropy
TrainHadronness.Metrics: accuracy, mae, mse
TrainHadronness.Optimizer: Adam
TrainHadronness.LearningRate: 0.1

# Size of the train data to use
TrainHadronness.TrainSize: 0.8
TrainHadronness.BatchSize: 1500000
TrainHadronness.Verbosity: 1
TrainHadronness.HistoryFile: history_hadronness.pkl
TrainHadronness.ScalerFile: hadronness_scaler.pickle

# Model Checkpoint
TrainHadronness.ModelCheckpoint.Disabled: no
#TrainHadronness.ModelCheckpoint.Path: energy-weights.{epoch:02d}-{val_loss:.2f}.hdf5
TrainHadronness.ModelCheckpoint.Path: hadronness-weights.hdf5
TrainHadronness.ModelCheckpoint.Verbose: 2
# If set to Yes, will only save the best (so far) values, otherwise will save all
TrainHadronness.ModelCheckpoint.SaveBestOnly: yes
# min, max, auto
TrainHadronness.ModelCheckpoint.Mode: auto
# val_loss, val_acc, val_mean_squared_error, val_mean_absolute_error
TrainHadronness.ModelCheckpoint.Monitor: val_loss
TrainHadronness.ModelCheckpoint.SaveWeightsOnly: yes 
TrainHadronness.ModelCheckpoint.Period: 1

# Early train stopping
TrainHadronness.EarlyStopping.MinDelta: 0.00001
# How many epochs to wait before stopping
TrainHadronness.EarlyStopping.Patience: 1500
# Verbosity level
TrainHadronness.EarlyStopping.Verbose: 1
TrainHadronness.EarlyStopping.Monitor: val_loss
TrainHadronness.EarlyStopping.Mode: min
# If disabled is set to True, the early stopping won't be applied
TrainHadronness.EarlyStopping.Disabled: no

# Reduce LR On Plateau
TrainHadronness.ReduceLROnPlateau.Factor: 0.05
TrainHadronness.ReduceLROnPlateau.Patience: 1000
TrainHadronness.ReduceLROnPlateau.MinLr: 0.0000000001
TrainHadronness.ReduceLROnPlateau.Verbose: 1
TrainHadronness.ReduceLROnPlateau.Disabled: no

# Number of entry variables
TrainHadronness.NumPar: 22
# Entry variables
TrainHadronness.InputPar0:MHillas_1.fSize
TrainHadronness.InputPar1:MHillas_1.fLength
TrainHadronness.InputPar2:MHillas_1.fWidth
TrainHadronness.InputPar3:MNewImagePar_1.fCoreArea
TrainHadronness.InputPar4:MNewImagePar_1.fLeakage1
TrainHadronness.InputPar5:MHillasTime_1.fRMSTimeW
TrainHadronness.InputPar6:MHillasTimeFit_1.fP1Grad
TrainHadronness.InputPar7:MHillas_2.fSize
TrainHadronness.InputPar8:MHillas_2.fLength
TrainHadronness.InputPar9:MHillas_2.fWidth
TrainHadronness.InputPar10:MNewImagePar_2.fCoreArea
TrainHadronness.InputPar11:MNewImagePar_2.fLeakage1
TrainHadronness.InputPar12:MHillasTime_2.fRMSTimeW
TrainHadronness.InputPar13:MHillasTimeFit_2.fP1Grad
TrainHadronness.InputPar14:MPointingPos_1.fZd
TrainHadronness.InputPar15:MStereoPar.fM1Impact
TrainHadronness.InputPar16:MStereoPar.fM2Impact
TrainHadronness.InputPar17:MStereoPar.fMaxHeight
TrainHadronness.InputPar18:MStereoPar.fCherenkovRadius
TrainHadronness.InputPar19:MStereoPar.fCherenkovDensity
# Number of Target variables

