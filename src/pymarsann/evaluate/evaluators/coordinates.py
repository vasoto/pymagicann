from .base import Evaluator
import ROOT as R
import time

class CoordinatesEvaluator(Evaluator):
    mm2deg = 0.00337
    extra_branches = ["MStereoPar", "MObservatory",
                      "MHillas_1", "MPointingPos_1",
                      "MGeomCam_1", "MSrcPosCam_1"]
    def __init__(self, manager, config):
        super(CoordinatesEvaluator, self).__init__(manager, config)
        self.headers_tree = None
        self.observatory = None
        self.stereo_param_ann = None
        self.geom_cam_1 = None
        self.geom_cam_2 = None

    def _set_stereo_param(self, x, y):
        stereo_input = self.manager.input_tree.MStereoPar
        # T1
        hillas_1 = self.manager.input_tree.MHillas_1
        pointing_pos_1 = self.manager.input_tree.MPointingPos_1
        src_pos_cam_1 = self.manager.input_tree.MSrcPosCam_1
        # T2
        hillas_2 = self.manager.input_tree.MHillas_2
        pointing_pos_2 = self.manager.input_tree.MPointingPos_2
        src_pos_cam_2 = self.manager.input_tree.MSrcPosCam_2
        self.stereo_param_ann.Calc_SrcDep(1,
                                            self.observatory,
                                            hillas_1,
                                            pointing_pos_1,
                                            self.geom_cam_1,
                                            src_pos_cam_1,
                                            2,
                                            self.observatory,
                                            hillas_2,
                                            pointing_pos_2,
                                            self.geom_cam_2,
                                            src_pos_cam_2)
        sX = self.mm2deg * src_pos_cam_1.GetX()
        sY = self.mm2deg * src_pos_cam_1.GetY()
        self.stereo_param_ann.SetDirectionX(x)
        self.stereo_param_ann.SetDirectionY(y)
        theta = ((sX - x) ** 2) + ((sY - y) ** 2)
        self.stereo_param_ann.SetTheta2(theta)
        self.stereo_param_ann.SetValidity(1)

    def process_event(self, event_id):
        start = time.time()
        self.stereo_param_ann.Reset()
        index = self.get_event_index(event_id)
        if index == -1:
            return False
        index = self.get_event_index(event_id)
        x = self.mm2deg * float(self.results[index][0])
        y = self.mm2deg * float(self.results[index][1])
        self._set_stereo_param(x, y)
        self.times.append(time.time() - start)
        return True

    def _set_run_headers(self):
        self.headers_tree = self.manager.input_root_file.Get('RunHeaders')
        self.headers_tree.SetBranchStatus("*", 1)
        self.headers_tree.GetEntry(0)
        self.observatory = self.headers_tree.MObservatory
        self.geom_cam_1 = self.headers_tree.MGeomCam_1
        self.geom_cam_2 = self.headers_tree.MGeomCam_2

    def setup_branches(self):
        output_branch = self.config.output_branch or "MStereoPar_DL"
        self.manager.input_tree.SetMakeClass(1)
        self.stereo_param_ann = R.MStereoPar(output_branch,
                                             "Coordinates stereo parameters "
                                             "estimated by DL")
        self.manager.output_tree.Branch(output_branch + ".",
                                        "MStereoPar",
                                        self.stereo_param_ann)
        self._set_run_headers()
