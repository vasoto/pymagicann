import logging
from sklearn.preprocessing import LabelEncoder
from .base import Evaluator
import ROOT as R
import time

class HadronnessEvaluator(Evaluator):
    def process_event(self, event_id):
        start = time.time()
        index = self.get_event_index(event_id)
        if index == -1:
            return False
        index = self.get_event_index(event_id)
        value = self.results[index]
#        print(value) #, self.label_encoder.inverse_transform([value]))
        self.hadronness.SetHadronness(value)
        self.hadronness_cls.SetHadronness(int(value > 0.5))
        self.times.append(time.time() - start)
        return True

    def predict(self):
        self.results = self.model.predict(self.data, verbose=2)
#        self.results = (results > 0.5).astype('int32')
#        self.results = self.scaler.inverse(results,
#                                           features=["Hadronness"])
    def setup_branches(self):
        # self.label_encoder = LabelEncoder()
        # self.label_encoder.fit([0.1, 0.9])
        
        output_branch = self.config.output_branch or "MHadronness_DL"
        logging.debug("Setting output branch for hadronness to {0}".format(output_branch))

        self.hadronness = R.MHadronness(output_branch,
                                        "Hadronness (raw), classified by Neural Net")
        self.manager.output_tree.Branch(output_branch + ".",
                                        "MHadronness",
                                        self.hadronness)
        self.hadronness_cls = R.MHadronness(output_branch + "_class.",
                                            "Hadronness (quantized), classified by Neural Net")
        self.manager.output_tree.Branch(output_branch + "_class.",
                                        "MHadronness",
                                        self.hadronness_cls)
