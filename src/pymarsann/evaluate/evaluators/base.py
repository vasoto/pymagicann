from argparse import Namespace
import copy
import pickle
import logging

import numpy as np
# from pymarsann.models.energy_simple import create_model
from pymarsann.data.preprocess._preprocess import DataPreprocessor
from pymarsann.models.io import load_model


logger = logging.getLogger('evaluator.base')


class Evaluator(object):
    
    extra_branches = []
    def __init__(self, manager, config):
        self.manager = manager
        self.config = config
        self.scaler = None
        self.model = None
        self.times = []
        self.preprocessor = None
        self.indexes = []
        if self.extra_branches:
            self.config._add_branches(self.extra_branches)
        self.next_index  = -1

    def _load_scaler(self):
        self.scaler = pickle.load(open(self.config.scaler_file, 'rb'))
        logger.debug("Scaler is loaded for {0}".
                     format(self.__class__.__name__))

    def _load_model(self):
        model_file = self.config.model_config.pop('model_file')
        self.model = load_model(model_file)


    def initialize(self):
        self._load_scaler()
        
        self.preprocessor = DataPreprocessor(self.config,
                                             inputs=self.config.inputs,
                                             targets=[],
                                             filters=self.config.filters,
                                             scaler=self.scaler,
                                             normalize_targets=True)
        self._load_model()
    
    def set_data(self, data):
        logger.info("Setting data for {0}".format(self.__class__.__name__))
        logger.debug("INPUTS:")
        for i, inpt in enumerate(self.config.inputs):
            logger.debug("\t{0}:\t{1}".format(i, inpt))
        logger.debug("FILTERS:")
        for i, filter in enumerate(self.config.filters):
            logger.debug("\t{0}:\t{1}".format(i, filter))
        
        logger.debug("BRANCHES:")
        for i, branch in enumerate(self.config.branches):
            logger.debug("\t{0}:\t{1}".format(i, branch))
        self.data = copy.copy(data[self.config.branches])

    def preprocess(self)->None:
        """ Preprocessing the data - running filters and applying functions
        on specified inputs.
        Normalization is done using saved scaler.
        """
        logger.debug("Preprocess {0}".format(self.__class__.__name__))
        orig_size = len(self.data)
        # indexing is needed to unpack the tuple
        self.data = self.preprocessor.fit(self.data)[0]
        self._set_indexes(orig_size)

    def _set_indexes(self, origin_data_size):
        indexes = -1 * np.ones(origin_data_size, dtype=np.int64)
        for i, index in enumerate(list(self.preprocessor.index_after_filter)):
            indexes[index] = i
        self.indexes = indexes.tolist()

    def predict(self)->None:
        # Predict
        logger.debug("Predict {0}".format(self.__class__.__name__))
        results = self.model.predict(self.data)
        # Inverse normalization
        logger.debug("Inverse normalization of predicted output for {0}".
            format(self.__class__.__name__))
        self.results = self.scaler.inverse(results,
                                           features=[br.replace('.', '_')
                                           for br in self.config.targets])
        logger.info("{0} finished prediction".format(self.__class__.__name__))


    def process_event(self, event_id: int) -> bool:
        pass
    
    def setup_branches(self) -> None:
        pass

    def get_event_index(self, event_id:int) -> int:
        return self.indexes[event_id]

    # def is_valid_event(self, event_id):
    #     return event_id in self.indexes

    # def get_next_index(self):
    #     # yield None
    #     # for i, index in enumerate(self.indexes):
    #     #     yield (i, index)
    #     return iter(enumerate(self.indexes))

    def get_stats(self):
        times = np.array(self.times)
        logger.debug("""Statistics for {0}:
        Number of events: {size}
        Mean time: {mean}
        Std. dev. time: {std}
        Total time: {time} sec""".
            format(self.__class__.__name__,
                   mean=times.mean(),
                   size=times.shape[0],
                   std=times.std(),
                   time=np.sum(times)))
