import logging
from .base import Evaluator
import ROOT as R
import time


class EnergyEvaluator(Evaluator):
    def _get_value(self, index):
        # Inverse log_10(x) --> 10**x
        return 10.0 ** float(self.results[index][0])

    def process_event(self, event_id):
        start = time.time()
        index = self.get_event_index(event_id)
        #print("{0} --> {1}".format(event_id, index))
        if index == -1:
            self.energy.SetEnergy(-1.0)
            return False
        self.energy.SetEnergy(self._get_value(index))
        self.times.append(time.time() - start)
        return True

    def setup_branches(self):
        output_branch = self.config.output_branch or "MEnergyEst_DL"
        logging.debug("Setting output branch for energy to {0}".format(output_branch))

        self.energy = R.MEnergyEst(output_branch,
                                    "Energy estimated by Neural Net")
        self.manager.output_tree.Branch(output_branch + ".",
                                        "MEnergyEst",
                                        self.energy)
