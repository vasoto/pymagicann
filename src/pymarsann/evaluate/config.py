import logging
from argparse import Namespace

from pymarsann.config import MarsConfigReader, MarsConfigBase, get_branches, append_list_params_to_list
from pymarsann.utils.text import str_to_bool

logger = logging.getLogger('evaluate_config')


class EvaluatorConfig(MarsConfigBase):
    def __init__(self,
                 scaler_file,
                 inputs,
                 filters,
                 targets,
                 model_config,
                 type: str,
                 output_branch=None):
        super(EvaluatorConfig, self).__init__()
        self.scaler_file = scaler_file
        self.inputs = inputs
        # self.outputs = outputs
        self.filters = filters
        self._check_filters()
        self.model_config = model_config
        # self.branches = self.inputs[:]
        self._add_branches(self.inputs)
        self._add_branches(self.filters)
        self.targets = targets
        self.output_branch = output_branch
        self.type = type
        # self._add_branches(self.outputs)

    @property
    def input_branches(self):
        return get_branches(self.inputs)

    @property
    def extra_branches(self):
        return

    @classmethod
    def from_dict(cls, config_dict):
        inputs = []
        append_list_params_to_list(config_dict, 'InputPar', inputs)
        targets = []
        append_list_params_to_list(config_dict, 'TargetPar', targets)

        # Load filters
        filters = []
        append_list_params_to_list(config_dict, 'Filter', filters)

        # load scaler file
        scaler_file = config_dict.pop('ScalerFile', 'scaler.pickle')
        type_ = config_dict.pop('Type', None)
        model_config = dict(
            model_file=config_dict.pop('ModelFile', 'model.model'),
            layers=[
                int(a) for a in config_dict.pop('Layers', '2,1').split(',')
            ],
            hidden_activation=config_dict.pop('HiddenActivation', 'relu'),
            output_activation=config_dict.pop('OutputActivation', 'linear'),
            kernel_normalizer=config_dict.pop('KernelNormalizer', 'normal'))
        output_branch = config_dict.pop('OutputBranch', None)

        return cls(scaler_file=scaler_file,
                   inputs=inputs,
                   filters=filters,
                   targets=targets,
                   model_config=model_config,
                   type=type_,
                   output_branch=output_branch)


class EvaluateConfig(MarsConfigReader):
    def __init__(self, config_file_name):
        super(EvaluateConfig, self).__init__(config_file_name)
        self.evaluators = dict()
        self.manager = dict()

    def _merge_config_lists(self, config_section):
        result = []
        for evaluator_conf in self.evaluators.values():
            attr = getattr(evaluator_conf, config_section, [])
            result = list(set(attr + result))
        result = get_branches(result)
        return result

    @property
    def input_branches(self):
        return self._merge_config_lists('inputs')

    def _process_evaluator(self, section_name):
        logger.debug("Processing evaluator {section_name}".format(
            section_name=section_name))
        eval_conf = EvaluatorConfig.from_dict(self.config[section_name])
        self._add_branches(eval_conf.branches)
        return eval_conf

    def _process_manager(self, section_name):
        logger.debug("Processing manager section")
        config = Namespace()
        config.input_files = self.config[section_name].pop(
            'InputFiles', '*.root')
        config.tree_name = self.config[section_name].pop(
            'RootTreeName', 'Events')
        config.num_events = int(self.config[section_name].pop('NumEvents', -1))
        config.output_dir = self.config[section_name].pop('OutputDir', '.')

        return config

    def load(self):
        super(EvaluateConfig, self).load()
        for section in self.config.keys():
            logger.info('Found section {section}'.format(section=section))
            if section.startswith('Evaluate'):
                enabled = str_to_bool(self.config[section].pop(
                    'Enabled', True))
                if enabled:
                    eval_config = self._process_evaluator(section)
                    self.evaluators[section.replace('Evaluate',
                                                    '')] = eval_config
                else:
                    logger.info(
                        'Section {0} is disabled. '
                        'In order to enable it set {0}.Enabled: Yes'.format(
                            section))
            elif section.startswith('Manager'):
                self.manager = self._process_manager(section)
        return self
