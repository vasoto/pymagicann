import glob
import logging
import os
import time

import numpy as np
import tqdm
import pandas
import ROOT as R

from .evaluators.coordinates import CoordinatesEvaluator
from .evaluators.energy import EnergyEvaluator
from .evaluators.hadronness import HadronnessEvaluator
from .config import EvaluateConfig

logger = logging.getLogger('evaluate_manager')


class EvaluateManager(object):
    EvaluatorClasses = {
        'energy': EnergyEvaluator,
        'coordinates': CoordinatesEvaluator,
        'hadronness': HadronnessEvaluator
    }

    def __init__(self, config):
        self.config = config
        self.evaluators = dict()
        self.scaler = None
        self.trees = {}

    @classmethod
    def from_config_file(cls, config_file_name):
        config = EvaluateConfig(config_file_name).load()
        return cls(config)

    def initialize(self):
        # Load data
        self.evaluators = dict()
        for name, conf in self.config.evaluators.items():
            if conf.type is None:
                raise AttributeError(
                    'Type for evaluator {0} is not set. Use one of {1}'.format(
                        name, list(self.EvaluatorClasses.keys())))
            self.evaluators[name] = self.EvaluatorClasses[conf.type.lower()](
                self, conf)
            self.evaluators[name].initialize()
            logger.info("Evaluator %s initialized", name)

    def _enumerate_trees(self):
        iterator = R.TIter(self.input_root_file.GetListOfKeys())
        self.trees = {}
        for key in iterator:
            tree_name = key.GetName()
            # Skip Events tree
            if tree_name == 'Events':
                continue

            try:
                tree = self.input_root_file.Get(tree_name).CloneTree()
                self.trees[tree_name] = tree
            except AttributeError as err:
                # Fails if "tree" object is not of type TTree
                logger.warning("Warning! Object {0}: {1}".format(
                    tree_name, str(err)))

    def _copy_trees(self):
        self._enumerate_trees()
        logger.debug("Found trees to copy: {0}".format(tuple(
            self.trees.keys())))
        for tree_name, tree in self.trees.items():
            logger.debug("Copy tree {0}".format(tree_name))
            tree.Write()

    def _initialize_input(self, input_file):
        # Open ROOT input file
        self.input_root_file = R.TFile(input_file)
        # Load tree
        self.input_tree = self.input_root_file.Get(
            self.config.manager.tree_name)
        # Enable branches
        self.input_tree.SetBranchStatus("*", 1)  # Enable all branches
        for branch in self.config.branches:
            self.input_tree.SetBranchStatus(branch, 1)

    def _load_data(self, input_file):
        self._initialize_input(input_file)
        num_events = self.input_tree.GetEntries()
        # Create an empty numpy array (faster than zeros)
        data = np.empty((num_events, len(self.config.branches)))
        logger.info("Reading {0} events...".format(num_events))
        # Read events and fill the selected branches into the data array
        for event_num in tqdm.tqdm(range(num_events)):
            self.input_tree.GetEntry(event_num)
            chunk = np.array([
                self.input_tree.GetLeaf(branch).GetValue()
                for branch in self.config.branches
            ])
            data[event_num, :] = chunk
        # Create pandas DataFrame from the data
        result = pandas.DataFrame(data, columns=self.config.branches)
        return result

    def evaluate(self):
        files_found = False
        for root_file in glob.glob(self.config.manager.input_files):
            files_found = True
            # Load input data from a ROOT file
            data = self._load_data(root_file)
            for evaluator in self.evaluators.values():
                # Copy the input data for a given evaluator
                evaluator.set_data(data)
                # Apply filters, functions and normalize inputs
                evaluator.preprocess()
                # Predict results and inverse normalize the outputs
                evaluator.predict()
            self.write_output(root_file)
        if not files_found:
            warning_msg = "No input files found -- {0}".format(
                self.config.manager.input_files)
            logger.warning(warning_msg)
            print("\n", warning_msg, "\n")

    def write_output(self, input_file):
        # Create output file name from the input file name
        output_file = os.path.basename(input_file).replace('_S_', '_Q_')
        # Prepend output directory to output file name
        output_file = os.path.join(self.config.manager.output_dir, output_file)
        # Create output ROOT file
        self.output_root_file = R.TFile(output_file, "RECREATE")
        # Copy all other trees to the output stream
        self._copy_trees()
        # Copy input tree into output file
        self.output_tree = self.input_tree.CloneTree(0)

        logger.info("Writing file {0}: This might take a while ...".format(
            output_file))
        # Setup and add branches to the output tree, for a given evaluator
        for evaluator in self.evaluators.values():
            evaluator.setup_branches()
        # Get number of events
        num_events = self.config.manager.num_events
        if num_events <= 0:
            num_events = self.input_tree.GetEntries()
        start = time.time()
        fill_times = []
        for event_index in tqdm.tqdm(range(num_events)):
            self.input_tree.GetEntry(event_index)  # Load data
            flag = True  # Set precessing flag
            for evaluator in self.evaluators.values():
                flag = flag and evaluator.process_event(event_index)
                if not flag:  # process event failed
                    break  # the event won't be filled
            else:
                fill_start = time.time()  # Start recording fill time
                self.output_tree.Fill()  # Fill the tree
                fill_times.append(time.time() - fill_start)  # Write fill time
        self.output_tree.Write()
        logger.info("Finished writing file {0}".format(output_file))
        logger.debug("Time filling the tree: {0:3.4} sec".format(
            np.sum(fill_times)))
        logger.debug(
            "Total time writing file: {0:3.4} sec".format(time.time() - start))
        for evaluator in self.evaluators.values():
            evaluator.get_stats()
