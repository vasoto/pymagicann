import argparse
import logging
import sys

from pymarsann.data.preprocess._preprocess import DataPreprocessor
from pymarsann.utils.root_utils import set_root_env
from pymarsann.evaluate.manager import EvaluateManager


logger = logging.getLogger('evaluate')
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s: [%(module)s] %(message)s")

def parse_arguments(args):
    parser = argparse.ArgumentParser(description="Evaluate data using given model.")
    parser.add_argument("-c",
                        "--config",
                        dest="config_file",
                        required=True,
                        help="Configuration file")
    return parser.parse_args(args)


def main(argv=sys.argv[1:]):
    set_root_env()
    args = parse_arguments(argv)
    manager = EvaluateManager.from_config_file(args.config_file)
    manager.initialize()
    manager.evaluate()
