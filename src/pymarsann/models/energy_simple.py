'''
Created on Jul 16, 2018

@author: vasot
'''
try:
    from tensorflow import keras
except Exception as _:
    import keras

#from keras.layers import Input, Dense, BatchNormalization, Dropout
#from keras.models import Model, Sequential


def create_model(config):
    """ Create simple fully connected model using configuration
    """
    # Get layers configuration from global configuration
    layers = config.layers
    # Ignoring first value in layers config, we take number of inputs 
    # from inputs configuration
    input_dim = len(config.inputs)
    # Create inputs layer
    inputs = keras.layers.Input(shape=(input_dim,), name='Inputs')
    x = inputs
    if config.batch_norm_after_inputs:
        # Add batch normalization after first layer
        x = keras.layers.BatchNormalization(name='NormalizationLayer')(x)
    # Add hidden layers
    for i, layer in enumerate(layers[1:-1]):
        x = keras.layers.Dense(layer,
                  activation=config.hidden_activation, 
                  kernel_initializer=config.kernel_normalizer, 
                  name='Layer{0}'.format(i + 1)
                  )(x)
        if config.batch_norm_after_hidden:
            x = keras.layers.BatchNormalization(
                        name='NormalizationLayer_{0}'.format(i))(x)
        if 0.0 < config.dropout_rate < 1.0:
            x = keras.layers.Dropout(rate=config.dropout_rate,
                        name="Dropout_{0}".format(i))(x)
    # Add output
    outputs = keras.layers.Dense(layers[-1],
                    activation=config.output_activation,
                    kernel_initializer=config.kernel_normalizer,
                    name='Output'
                    )(x)
    # Generate model
    model = keras.models.Model(inputs=[inputs],
                               outputs=[outputs])
    return model
