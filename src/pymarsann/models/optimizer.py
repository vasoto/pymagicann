from tensorflow.keras.optimizers import Adam, SGD, RMSprop, Adagrad, \
    Adamax, Adadelta, Nadam
from functools import partial

Optimizers = {
        'sgd': SGD,
        'rmsprop': RMSprop,
        'adagrad': Adagrad,
        'adadelta': Adadelta,
        'adam': Adam,
        'adam_with_decay': partial(Adam, decay=1e-6),
        'adamax': Adamax,
        'nadam': Nadam,
    }


def get_optimizer(config):
    optimizer_cls = Optimizers.get(config.optimizer.lower(), None)
    return optimizer_cls(lr=config.learning_rate)

