import time
import numpy as np
from tensorflow.keras.callbacks import Callback


class TimeHistory(Callback):
    def __init__(self, report_period=100):
        super(TimeHistory, self).__init__()
        self.current_epoch = 0
        self.report_period = report_period

    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.current_epoch += 1
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        diff = time.time() - self.epoch_time_start
        self.times.append(diff)
        if self.current_epoch % self.report_period == 0:
            print("Epoch {current_epoch}: Avg. time for last {report_period} epochs is {diff:3.6}".format(diff=np.array(self.times).mean(),
            report_period=self.report_period,
            current_epoch=self.current_epoch))
