import json
import logging
from argparse import Namespace
from pathlib import Path
from .energy_simple import create_model
from typing import Union


def dump_model(model, config, model_file):
    weights_file = "{0}.weights".format(model_file)
    cfg = dict(layers=config.layers,
               inputs=config.inputs,
               batch_norm_after_inputs=config.batch_norm_after_inputs,
               hidden_activation=config.hidden_activation,
               kernel_normalizer=config.kernel_normalizer,
               batch_norm_after_hidden=config.batch_norm_after_hidden,
               dropout_rate=config.dropout_rate,
               output_activation=config.output_activation,
               weights_file=weights_file
               )
    model.save_weights(weights_file)
    json.dump(cfg, open("{0}.config".format(model_file), 'w'))


def load_model(model_file:Union[str, Path])->"keras.Model":
    if not isinstance(model_file, Path):
        model_file = Path(model_file)
    pre_suffix = ''
    if model_file.suffix != '.config':
        pre_suffix= model_file.suffix
    # Set the approprate extension for the config file
    config_file = model_file.with_suffix('{0}.config'.format(pre_suffix))

    logging.debug("Trying to load model config from "
                  "{config_file}".format(config_file=config_file))
    cfg = json.load(open(config_file))
    cfg = Namespace(**cfg)
    model = create_model(cfg)

    # Load weights
    try:
        weights_file = config_file.with_suffix('.hdf5')
        if not weights_file.exists():
            old_weights_file = weights_file.with_suffix('.weights')
            old_weights_file.rename(weights_file)
    except FileNotFoundError as err:
        weights_file = config_file.with_suffix('.weights')

    logging.debug("Trying to load model weights from %s", weights_file)
    try:
        model.load_weights(str(weights_file))
    except Exception as err:
        logging.error("Error while loading model weights: %s", err)
        raise err
    return model
