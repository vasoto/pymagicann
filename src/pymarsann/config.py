'''
Created on Jul 16, 2018

@author: vasot
'''
import ast
from argparse import Namespace
import os
import re
import glob

from pymarsann.utils.text import str_to_bool


def append_list_params_to_list(config, prefix, container, del_param=True):
    i = 0
    while "{prefix}{i}".format(prefix=prefix, i=i) in config:
        container.append(config["{prefix}{i}".format(prefix=prefix, i=i)])
        if del_param:
            del config["{prefix}{i}".format(prefix=prefix, i=i)]
        i += 1

def get_branches(aliases):
    """Get unique branch names from an alias dictionary.
    """
    ignore = ['pow', 'log10', 'sqrt', 'max']
    branches = []
    for value in aliases:
        tokens = re.sub('[\(\)\+\*\/\,\=\<\>\&\!\-\|]', ' ', value).split()
        for t in tokens:
            if bool(re.search(r'^\d', t)) or len(t) <= 3:
                continue
            if bool(re.search(r'[a-zA-Z]', t)) and t not in ignore:
                branches += [t]
    return list(set(branches))


class MarsConfigBase(object):
    def __init__(self):
        self.config = dict()
        self.branches = []
        self.filters = []

    def _check_filters(self):
        substitute_map = {'&&' : 'and', '||' : 'or'}
        for i, fltr in enumerate(self.filters):
            for v1, v2 in substitute_map.items():
                fltr = fltr.replace(v1, v2)
            self.filters[i] = "({fltr})".format(fltr=fltr)

    def _get_key_value(self, line):
        key, value = line.split(':', 1)
        key = key.strip()
        value = value.strip()
        return key, value
    
    def _split_key_and_assign(self, key, value):
        keys = key.split('.')
        d = self.config
        for k in keys[:-1]:
            if k not in d:
                d[k] = dict()
            d = d[k]
        d[keys[-1]] = value
    
    def _add_branches(self, params):
        input_par = self.branches
        for inpar in params:
            tree = ast.parse(inpar)
            ParameterVisitor(input_par).visit(tree)
        self.branches = list(set(input_par))


class MarsConfigReader(MarsConfigBase):
    def __init__(self, config_file_name):
        super(MarsConfigReader, self).__init__()
        self.config_file_name = config_file_name
    
    def load(self):
        with open(self.config_file_name, 'r') as config_file:
            for line in config_file:
                # Remove whitespace
                line = line.strip()
                # Skip comments or empty lines
                if not line or line.startswith('#'):
                    continue
                try:
                    key, value = self._get_key_value(line)
                except Exception as err:
                    print("Error in line: {0}".format(line))
                    raise err
                self._split_key_and_assign(key, value)
        return self


class ParameterVisitor(ast.NodeVisitor):
    def __init__(self, container):
        self.container = container

    def visit_Attribute(self, node):
        parameter_name = "{id}.{attr}".format(id=node.value.id,
                                              attr=node.attr)
        self.container.append(parameter_name)
        return node


class TrainingConfig(MarsConfigReader):
    def __init__(self, config_file_name):
        super(TrainingConfig, self).__init__(config_file_name)
        self.filters = []
        self.inputs = []
        self.outputs = []
        self.targets = []
        self.unbias_params = []
        self.branches = []
        self.early_stopping = {}
        self.layers = None
        self.normalizer = None
        self.epochs = 0
        self.model_type = None
        self.tree_name = ""
        self.train_files = None
        self.input_files = ''
        self.output_dir = ''
        self.hidden_activation = ''
        self.output_activation = ''
        self.kernel_normalizer = ''
        self.model_output_prefix = ''
        self.model_file = ''
        self.scaler_file = ''
        self.train_size = 0.0
        self.batch_size = 0
        self.verbosity = 0
        self.reduce_lr_on_plateau = Namespace()
        self.model_checkpoint = Namespace()
        self.history_file = ''
        self.optimizer = ''
        self.loss = ''
        self.metrics = ''
        self.stratify = False
        self.undersample = False

    def __str__(self):
        return """Config:
        Model: {model_type}
        Layers: {layers}
        Epochs: {epochs}
        Train files: {train_files}
        Hidden activation: {hidden_activation}
        Output activation: {output_activation}
        Kernel normalizer: {kernel_normalizer}
        Tree: "{tree_name}"
        Filters: {filters}
        Branches: {branches}
        Inputs: {inputs}
        Targets: {targets}
        Outputs: {outputs}
        Normalize: {normalizer}""".format(**self.__dict__)
    
    @property
    def target_branches(self):
        return get_branches(self.targets)

    @property
    def input_branches(self):
        return get_branches(self.inputs)

    def _extract_files(self, config, prefix):
        def _exists(files):
            return os.path.exists(files) or len(glob.glob(files))
        token = "{prefix}Files".format(prefix=prefix)
        if token in config:
            _files = config.pop(token)
            if _exists(_files):
                return _files
            folder = os.path.dirname(self.config_file_name)
            files = os.path.join(folder, _files)
            if not _exists(files):
                raise ValueError("No files found for {token}={_files}".
                                 format(token=token, _files=_files))
            return files
        return None

    def _set_early_stopping(self, config):
        early_stopping = config.pop('EarlyStopping', {})
        self.early_stopping = Namespace()
        self.early_stopping.monitor = early_stopping.get('Monitor', 'val_loss')
        self.early_stopping.disabled = str_to_bool(early_stopping.get('Disabled', True))
        self.early_stopping.min_delta = float(early_stopping.get('MinDelta', 0.01))
        self.early_stopping.patience = int(early_stopping.get('Patience', 50))
        self.early_stopping.verbose = float(early_stopping.get('Verbose', 0))
        self.early_stopping.mode = early_stopping.get('Mode', 'auto')

    def _set_reduce_lr_on_plateau(self, config):
        reduce_lr_on_plateau = config.pop('ReduceLROnPlateau', {})
        self.reduce_lr_on_plateau = Namespace()
        self.reduce_lr_on_plateau.disabled = str_to_bool(reduce_lr_on_plateau.get('Disabled', True))
        self.reduce_lr_on_plateau.factor = float(reduce_lr_on_plateau.get('Factor', 0.1))
        self.reduce_lr_on_plateau.patience = int(reduce_lr_on_plateau.get('Patience', 50))
        self.reduce_lr_on_plateau.min_lr = float(reduce_lr_on_plateau.get('MinLr', 0.0001))
        self.reduce_lr_on_plateau.verbose = int(reduce_lr_on_plateau.get('Verbose', 0))

    def _set_model_checkpoint(self, config):
        model_checkpoint = config.pop('ModelCheckpoint', {})
        self.model_checkpoint = Namespace()
        self.model_checkpoint.disabled = str_to_bool(model_checkpoint.get('Disabled', True))
        self.model_checkpoint.filepath = model_checkpoint.get('Path', 'energy-weights.{epoch:02d}-{val_loss:.2f}.hdf5')
        self.model_checkpoint.filepath = os.path.join(self.output_dir, self.model_checkpoint.filepath)
        self.model_checkpoint.verbose = int(model_checkpoint.get('Verbose', 1))
        self.model_checkpoint.save_best_only = str_to_bool(model_checkpoint.get('SaveBestOnly', False))
        self.model_checkpoint.mode = model_checkpoint.get('Mode', 'auto')
        self.model_checkpoint.monitor = model_checkpoint.get('Monitor', 'val_loss')
        self.model_checkpoint.save_weights_only = str_to_bool(model_checkpoint.get('SaveWeightsOnly', True))
        self.model_checkpoint.period = int(model_checkpoint.get('Period', 1))

    def _set_files(self, config):
        self.train_files = self._extract_files(config, 'Train')

    def _set_parameters(self, config):
        # Namespaces are one honking great idea -- let's do more of those!
        # Layers
        self.layers = [int(a) for a in config.pop('Layers', '2,1').split(',')]
        # Epochs
        self.epochs = int(config.pop('Epochs', 0))
        # Model Type
        self.model_type = config.pop('Type').lower()
        # Normalization
        normalizer = config.pop('Normalizer', dict())
        self.normalizer = Namespace(min=float(normalizer.get('Min', 0.0)),
                                    max=float(normalizer.get('Max', 1.0)))
        self.tree_name = config.pop('TreeName', 'Events')
        
        # self.test_files = self._extract_files(config, 'Test')
        self.hidden_activation = config.pop('HiddenActivation', 'relu')
        self.output_activation = config.pop('OutputActivation', 'sigmoid')
        self.kernel_normalizer = config.pop('KernelNormalizer', 'normal')
        self.batch_norm_after_inputs = str_to_bool(config.pop('BatchNormAfterInputs', False))
        self.batch_norm_after_hidden = str_to_bool(config.pop('BatchNormAfterHidden', False))
        self.dropout_rate = float(config.pop('DropoutRate', 1.0))
        self.model_output_prefix = config.pop('ModelOutputPrefix', 'model')
        self.model_file = config.pop('ModelOutputFile', 'model.pickle')
        self.scaler_file = config.pop('ScalerFile', 'scaler.pickle')
        self.train_size = float(config.pop('TrainSize', 0.8))
        self.batch_size = int(config.pop('BatchSize', 1024))
        self.verbosity = int(config.pop('Verbosity', 1))
        self.optimizer = config.pop('optimizer', 'Adam')
        self.learning_rate = float(config.pop('LearningRate', 0.1))
        self.history_file = config.pop('HistoryFile', 'history.pickle')
        default_output_dir = './'
        self.output_dir = config.pop('OutputDir', default_output_dir)
        if self.output_dir != default_output_dir:
            if not os.path.exists(self.output_dir):
                os.mkdir(self.output_dir)
            self.history_file = os.path.join(self.output_dir, self.history_file)
            self.model_file = os.path.join(self.output_dir, self.model_file)
            self.scaler_file = os.path.join(self.output_dir, self.scaler_file)
        self.stratify = bool(config.pop('StratifyData', False))
        self.undersample = bool(config.pop('Undersample', False))

        self.loss = config.pop('Loss', 'mse')
        self.metrics = [metric.strip()
                        for metric in config.pop('Metrics',
                                                 self.loss).split(',')]

    def load(self):
        super(TrainingConfig, self).load()
        config = None
        for key in self.config.keys():
            if key.startswith('Train'):
                config = self.config[key]
        if not config:
            raise ValueError("Cannot find configuration section starting with 'Train'")

        append_list_params_to_list(config, 'Filter', self.filters)
        self._check_filters()
        self._add_branches(self.filters)
        append_list_params_to_list(config, 'InputPar', self.inputs)
        self._add_branches(self.inputs)
        append_list_params_to_list(config, 'TargetPar', self.targets)
        self._add_branches(self.targets)
        append_list_params_to_list(config, 'OutputPar', self.outputs)
        self._add_branches(self.outputs)
        append_list_params_to_list(config, 'UnbiasPar', self.unbias_params)
        self._set_parameters(config)
        self._set_files(config)
        self._set_model_checkpoint(config)
        self._set_early_stopping(config)
        self._set_reduce_lr_on_plateau(config)
        return self


class TrainingConfigHadronness(TrainingConfig):
    def __init__(self, config_file_name):
        super(TrainingConfigHadronness, self).__init__(config_file_name)
        del self.train_files
        self.hadron_files = None
        self.gamma_files = None

    def _set_files(self, config):
        self.proton_files = self._extract_files(config, 'ProtonTrain')
        self.gamma_files = self._extract_files(config, 'GammaTrain')
