import warnings
warnings.filterwarnings("ignore")

import argparse
import logging
import pickle
import sys

# Color logging to stdout/stderr
try:
    import coloredlogs
    coloredlogs.install(level='DEBUG')
except Exception as _:
    pass

try:
    from tensorflow.keras.callbacks import EarlyStopping
except Exception as _:
    from keras.callbacks import EarlyStopping

from sklearn.model_selection import train_test_split


from pymarsann.config import TrainingConfig
from pymarsann.models.io import load_model
from pymarsann.models.optimizer import get_optimizer
from pymarsann.data.preprocess._preprocess import DataPreprocessor

# Setup logging
logger = logging.getLogger('train_test')
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s: [%(module)s] %(message)s")


import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'  # suppress CPU msg


def parse_arguments(args=sys.argv[1:]):
    ''' Parse command line arguments
    '''
    parser = argparse.ArgumentParser(description="Test trained model.")
    parser.add_argument("-c",
                        "--config",
                        dest="config_file",
                        required=True,
                        help="Configuration file")
    parser.add_argument("-m",
                        "--model",
                        dest="model_file",
                        required=False,
                        default=None,
                        help="Explicit model file")
    return parser.parse_args(args)


def main(load_data_fn, cfg_class=TrainingConfig, stratify=None):
    logging.disable(logging.DEBUG)
    args = parse_arguments()
    config = cfg_class(args.config_file).load()
    logger.info("Configuration is loaded.")
    train_data = load_data_fn(config)
    scaler = pickle.load(open(config.scaler_file, 'rb'))
    preprocessor = DataPreprocessor(config,
                                    inputs=config.inputs,
                                    targets=config.targets,
                                    filters=config.filters,
                                    scaler=scaler,
                                    normalize_targets=True)

    X, y = preprocessor.fit(train_data)

    # Split training data
    if stratify:
        stratify = y
    x_train, x_validation, y_train, y_validation = train_test_split(
        X,
        y,
        train_size=0.8,
        stratify=stratify)
    model_file = args.model_file
    if not model_file:
        model_file = config.model_file
    model = load_model(model_file)
    optimizer = get_optimizer(config)
    model.compile(loss=config.loss,
                  optimizer=optimizer,
                  metrics=config.metrics + ['mae'])
    model.summary()
    print("\nEvaluating train set...")
    res = model.evaluate(x_train,
                         y_train,
                         batch_size=config.batch_size,
                         verbose=1,)
    print("Metrics:")
    for metric in zip(model.metrics_names, res):
        print("{0}: {1:2.6}".format(*metric))
    # print(dir(model))
    print("\nEvaluating validation set...")
    res = model.evaluate(x_validation,
                         y_validation,
                         batch_size=config.batch_size,
                         verbose=1,)
    print("Metrics:")
    for metric in zip(model.metrics_names, res):
        print("{0}: {1:2.6}".format(*metric))
    # for line in dir(model):
    #     print(line)
    # print(model.input_names)
