import logging
import pickle

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from pymarsann.data.reader import UpRootReader
from pymarsann.data.preprocess._preprocess import DataPreprocessor

logger = logging.getLogger('classification')


def get_preprocessor(config, train_data):
    preprocessor = DataPreprocessor(config=config,
                            inputs=config.inputs,
                            targets=['Hadronness'],
                            filters=config.filters,
                            drop_duplicates=None,
                            normalize_targets=True
                            )
    X, y = preprocessor.fit(train_data)
    encoder = LabelEncoder()
    encoder.fit(y)
    logger.debug("Label encoder classes: {0}".format(encoder.classes_))
    encoded_y = encoder.transform(y)
    return X, encoded_y, preprocessor

def load_data(config):
    logger.info('Loading classification data for classes [hadrons, gamma]')
    reader = UpRootReader(config)
    gamma_data = reader.load(config.gamma_files)
    gamma_data['Hadronness'] = pd.Series(np.zeros(len(gamma_data)),
                                         index=gamma_data.index)
    # print(gamma_data.head(10))
    hadron_data = reader.load(config.proton_files)
    hadron_data['Hadronness'] = pd.Series(np.ones(len(hadron_data)),
                                          index=hadron_data.index)
    data = pd.concat((gamma_data, hadron_data), ignore_index=True)
    data = data.sample(frac=1).reset_index(drop=True)
    # Remove data to save memory
    del hadron_data
    del gamma_data
    return data
