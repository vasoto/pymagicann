import logging
import pickle

from pymarsann.data.reader import UpRootReader
from pymarsann.data.preprocess._preprocess import DataPreprocessor

logger = logging.getLogger('regression')

def get_preprocessor(config, train_data):
    preprocessor = DataPreprocessor(config=config,
                            inputs=config.inputs,
                            targets=config.targets,
                            filters=config.filters,
                            drop_duplicates=None, # TODO: Fetch from configuration #config.target_branches
                            normalize_targets=True # TODO: fetch from configuration
                            )
    X, y = preprocessor.fit(train_data)
    return X, y, preprocessor


def load_data(config):
    # Load training and testing data
    logger.info("Loading training data (this process might take a while)")
    reader = UpRootReader(config)
    train_data = reader.load(config.train_files)
    logger.info("Training data is loaded.")
    return train_data