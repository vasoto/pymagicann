import argparse
import logging
import pickle
import sys
import pandas
# Color logging to stdout/stderr
try:
    import coloredlogs
    coloredlogs.install(level='DEBUG')
except Exception as _:
    pass

#from keras.optimizers import Adam, SGD
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

from sklearn.model_selection import train_test_split


from pymarsann.config import TrainingConfig


from pymarsann.models.energy_simple import create_model
from pymarsann.models.io import dump_model
from pymarsann.models.callbacks.timer import TimeHistory
from pymarsann.models.optimizer import get_optimizer

# Setup pandas output
pandas.set_option('display.max_rows', 500)
pandas.set_option('display.max_columns', 500)
pandas.set_option('display.width', 2000)

# Setup logging
logger = logging.getLogger('train')
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s: [%(module)s] %(message)s")


def parse_arguments(args=sys.argv[1:]):
    ''' Parse command line arguments
    '''
    parser = argparse.ArgumentParser(description="Train a model.")
    parser.add_argument("-c",
                        "--config",
                        dest="config_file",
                        required=True,
                        help="Configuration file")
    return parser.parse_args(args)


def main(load_data_fn, preprocessor_fn, cfg_class=TrainingConfig):
    args = parse_arguments()
    # Load configuration from file
    config = cfg_class(args.config_file).load()
    logger.info("Configuration is loaded.")
    train_data = load_data_fn(config)
    
    X, y, preprocessor = preprocessor_fn(config, train_data)

    logger.info("Saving scaler data to {0}".format(config.scaler_file))
    # Save scaler
    with open(config.scaler_file, 'wb') as scaler_pickle_file:
        pickle.dump(preprocessor.scaler,
                    scaler_pickle_file,
                    protocol=2)
    # Undersample data
    if config.undersample:
        from collections import Counter
        from imblearn.under_sampling import RandomUnderSampler # pick undersampled data at random
        
        logger.info("Undersamping is enabled")
        orig_size = X.shape[0]
        logger.info('Data size before undersampling: {0}'.format(orig_size))
        logger.debug("Samples per class (before): {0}".format(sorted(list(Counter(y).items()))))
        rus = RandomUnderSampler(random_state=0)
        X, y = rus.fit_resample(X, y)
        logger.info('Data size after undersampling: '
                    '{0}. {1} samples removed'.format(
                        X.shape[0],
                        orig_size - X.shape[0]))
        logger.debug("Samples per class (after): "
                     "{0}".format(sorted(list(Counter(y).items()))))
    # Split training data
    stratify = None
    if config.stratify:
        logger.info("Stratification is enabled")
       
        stratify = y
    x_train, x_validation, y_train, y_validation = train_test_split(
        X,
        y,
        train_size=config.train_size,
        stratify=stratify)
    # Create model
    model = create_model(config)
    # Setup
    optimizer = get_optimizer(config)
    logger.debug("Compiling model with loss={loss}; "
                 "metrics={metrics}; "
                 "optimizer={optimizer}; "
                 "learning rate={learning_rate}".format(**vars(config)))

    model.compile(loss=config.loss,
                  optimizer=optimizer,
                  metrics=config.metrics)

    callbacks_list = []
    if not config.model_checkpoint.disabled:
        checkpoint = ModelCheckpoint(
            config.model_checkpoint.filepath,
            monitor=config.model_checkpoint.monitor,
            verbose=config.model_checkpoint.verbose,
            save_best_only=config.model_checkpoint.save_best_only,
            mode=config.model_checkpoint.mode,
            period=config.model_checkpoint.period,
            save_weights_only=True, # Save the whole model
            #config.model_checkpoint.save_weights_only
            )
        callbacks_list.append(checkpoint)
    else:
        logger.info("Model checkpoint is disabled. "
                    "Model will be saved once, when training is completed.")

    if not config.early_stopping.disabled:
        earlystop = EarlyStopping(monitor=config.early_stopping.monitor,  # 'val_mean_absolute_error',
                                  min_delta=config.early_stopping.min_delta,
                                  patience=config.early_stopping.patience,
                                  verbose=config.early_stopping.verbose,
                                  mode=config.early_stopping.mode)
        callbacks_list.append(earlystop)
    else:
        logger.info('Early stopping is disabled. '
                    'The model will train for '
                    '{config.epochs} epochs'.format(config=config))
    if not config.reduce_lr_on_plateau.disabled:
        reduce_lr = ReduceLROnPlateau(
            factor=config.reduce_lr_on_plateau.factor,
            patience=config.reduce_lr_on_plateau.patience,
            min_lr=config.reduce_lr_on_plateau.min_lr,
            verbose=config.reduce_lr_on_plateau.verbose)
        callbacks_list.append(reduce_lr)
    else:
        logger.info('Reduce learning rate on plateau is disabled.')

    time_callback = TimeHistory()
    callbacks_list.append(time_callback)
    model.summary()
    history = model.fit(x_train,
                        y_train,
                        validation_data=(x_validation, y_validation),
                        epochs=config.epochs,
                        batch_size=config.batch_size,
                        callbacks=callbacks_list,
                        verbose=config.verbosity)
    n_epochs = len(history.history['loss'])
    logger.info("Training stopped after {0} epochs".format(n_epochs))
    # Save final model
    dump_model(model, config, '{0}.final'.format(config.model_file))
    #dump_model(model, '{0}.final'.format(config.model_file))
    # Save best model
    if not config.model_checkpoint.disabled:
        model.load_weights(config.model_checkpoint.filepath) # Load best weights
        dump_model(model, config, "{0}.best".format(config.model_file))
    # Dump either the best or last model
    # dump_model(model, "{0}.best".format(config.model_file))
    # Save history to csv
    pandas.DataFrame(history.history).to_csv(config.history_file)
    # with open(config.history_file, 'wb') as hist_file:
    #     pickle.dump(history.history, hist_file)
