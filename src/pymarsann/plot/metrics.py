'''
Created on Aug 13, 2018

@author: vasot
'''
import matplotlib.pyplot as plt
import pickle
import argparse
import sys


def learning_rate_plot(data, ax):
    ax.set_title("Learning rate")
    ax.set_xlabel("Epochs")
    ax.plot(data["lr"],
            label="Learning rate change")
    ax.legend()


def metrics_plot(data, metrics, ax, y_log_scale=False, legend=True, start=None, end=None):
    metrics_label = metrics.replace("_", " ").replace("val_", '')
    x = list(range(len(data[metrics])))
    x = x[start:end]
    ax.set_title(metrics_label.capitalize())
    ax.plot(x, data[metrics][start:end],
                 label="Train {metrics_label}".format(metrics_label=metrics_label))
    ax.plot(x, data["{metrics}".format(metrics=metrics)][start:end],
                 label="Validation loss")
    ax.set_xlabel("Epochs")
    if y_log_scale:
        ax.set_yscale("log", nonposy='clip')
    if legend:
        ax.legend()
    return ax

def plot_all_metrics(history, start=None, end=None, y_log_scale=False):
    """ Plot all metrics in Keras model training history.history dictionary
    """
    fig = plt.figure(figsize=(20,10))

    metrics = dict()
    for metric in history.keys():
        if metric.lower() in ('lr'):
            continue
        metrics[metric] = plt.figure(figsize=(20,10)).add_subplot(111)
    for metric, ax in metrics.items():
        if metric in history:
            metrics_plot(history,
                         metric,
                         ax,
                         start=start,
                         end=end,
                         y_log_scale=y_log_scale)
    ax_lr = plt.figure(figsize=(20,10)).add_subplot(111)
    if 'lr' in history.keys():
        learning_rate_plot(history, ax_lr)
    
    fig.suptitle('Train metrics')
    fig.subplots_adjust(hspace=0.4)


def _parse_arguments(args):
    ap = argparse.ArgumentParser(description=
            "Plot training metrics from a pickled history file.")
    ap.add_argument("-b",
                    "--begin",
                    dest='start',
                    default=None,
                    type=int,
                    help="Epoch to begin plotting from")
    ap.add_argument("-e",
                    "--end",
                    dest='end',
                    default=None,
                    type=int,
                    help="Epoch to end plotting to")
    ap.add_argument('--logy',
                    action='store_true',
                    dest='y_log_scale',
                    help='Log scale on y-axis')
    ap.add_argument('-o',
                    '--output',
                    default=None,
                    dest='output_file',
                    help='Output file to save the plot.')
    ap.add_argument("history_file",
                    type = lambda file_name: open(file_name, 'rb'),
                    help="History pickled file to load metrics from.")
    return ap.parse_args(args)


def main(argv=sys.argv[1:]):
    args = _parse_arguments(argv)
    # print(vars(args))
    history = pickle.load(args.history_file)
    print(history.keys())
    plot_all_metrics(history,
                     start=args.start,
                     end=args.end,
                     y_log_scale=args.y_log_scale,
                     )
    if args.output_file:
        plt.savefig(args.output_file, dpi=90)
    plt.tight_layout()
    plt.show()
