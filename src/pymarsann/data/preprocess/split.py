'''
Created on Aug 13, 2018

@author: vasot
'''
import numpy as np


def split_balanced(X, y, range_=(0.15, 0.9), test_size=0.2):
    _, bins = np.histogram(y,
                           bins='fd', #num_classes-1,
                           range=range_
                           )
    groups = np.digitize(y, bins=bins).squeeze()
    classes = np.unique(groups)
    size = len(y)
    # can give test_size as fraction of input data size of number of samples
    if test_size < 1:
        n_test = np.round(size * test_size)
    else:
        n_test = test_size
    n_train = max(0, size - n_test)
    n_train_per_class = max(1, int(np.floor(n_train / len(classes))))
    n_test_per_class = max(1, int(np.floor(n_test / len(classes))))

    ixs = []
    for cl in classes:
        sum_ = np.sum(groups==cl)
        if (n_train_per_class + n_test_per_class) > sum_:
            # if data has too few samples for this class, do upsampling
            # split the data to training and testing before sampling so data points won't be
            #  shared among training and test data
            splitix = int(np.ceil(n_train_per_class / 
                                  (n_train_per_class + n_test_per_class) * 
                                  sum_))
            ixs.append(np.r_[np.random.choice(np.nonzero(groups==cl)[0][:splitix],
                                              n_train_per_class),
                np.random.choice(np.nonzero(groups==cl)[0][splitix:],
                                 n_test_per_class)])
        else:
            ixs.append(np.random.choice(np.nonzero(groups==cl)[0],
                                        n_train_per_class + n_test_per_class,
                replace=False))

    # take same num of samples from all classes
    ix_train = np.concatenate([x[:n_train_per_class] 
                               for x in ixs])
    ix_test = np.concatenate([x[n_train_per_class:(n_train_per_class +
                                                    n_test_per_class)] 
                              for x in ixs])

    X_train = X[ix_train, :]
    X_test = X[ix_test, :]
    y_train = y[ix_train]
    y_test = y[ix_test]

    return X_train, X_test, y_train, y_test
