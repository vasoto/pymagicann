'''
Created on Jul 16, 2018

@author: vasot
'''
import gc
import logging

from sklearn.preprocessing import MinMaxScaler
from sklearn.base import TransformerMixin
import numpy as np

from pymarsann.utils.mem import get_mem

logger = logging.getLogger('normalize')

def merge_scalers(scaler1, scaler2):
    """ Merge two Scalers
    """
    scaler = scaler1
    scaler.features = scaler1.features + scaler2.features
    scaler.scaler.min_ = np.concatenate((scaler1.scaler.min_,
                                         scaler2.scaler.min_))
    scaler.scaler.scale_ = np.concatenate((scaler1.scaler.scale_,
                                           scaler2.scaler.scale_))
    scaler.scaler.data_min_ = np.concatenate((scaler1.scaler.data_min_,
                                              scaler2.scaler.data_min_))
    scaler.scaler.data_max_ = np.concatenate((scaler1.scaler.data_max_,
                                              scaler2.scaler.data_max_))
    scaler.scaler.data_range_ = np.concatenate((scaler1.scaler.data_range_,
                                                scaler2.scaler.data_range_))
    return scaler


class Scaler(TransformerMixin):
    def __init__(self, config, features=None, scaler=None):
        self.config = config
        self.scaler = scaler
        self.features = features or []
        
    def fit(self, X):
        logger.debug("Scaler fit begin")
        X = np.asanyarray(X)
        if not self.scaler:
            self.scaler = MinMaxScaler((self.config.normalizer.min,
                                        self.config.normalizer.max),
                                       copy=False)
        self.scaler.copy = False # Explicitly turn off copying of data for scalers
        self.scaler.fit(X)
        logger.debug("Scaler fitting done")
        total = gc.collect()
        logger.debug("Collected {total} objects".format(total=total))
        return self
    
    def transform(self, X, y=None, features=None):
        logger.debug("Scaler transform begin")
        X = np.asanyarray(X)#[:]  # .values
        gc.collect()
        logger.debug("X is transformed to numpy array")
        logger.debug("Memory usage: {0}".format(get_mem()/(1024*1024*1024)))
        features = features or self.features
        #if features:
        logger.debug("Scaler transformation using features")
        indexes = [self.features.index(feature)
                   for feature in features]
        # Multiply inplace
        np.multiply(X, self.scaler.scale_[indexes], out=X)
        #x *= self.scaler.scale_[indexes]
        logger.debug("Scaling transform done.")
        X += self.scaler.min_[indexes]
        logger.debug("Offset transform done.")
        #else:
        #X = self.scaler.transform(X)
        return X
    
    def inverse(self, Z, features=None):
        logger.debug("Selected features for inverse transformation are: {0}".format(features))
        z = np.asanyarray(Z).copy()
        if features:
            logger.debug("Scaler inverse transformation using features")
            indexes = [self.features.index(feature)
                       for feature in features]
            z -= self.scaler.min_[indexes]
            z /= self.scaler.scale_[indexes]
        else:
            z = self.scaler.inverse_transform(z)
        return z
    
    def _get_feature_index(self, feature_name):
        return self.features.index(feature_name)


def normalize_events(events, limits=(0.1, 0.9), scaler=None):
    if scaler is None:
        scaler = MinMaxScaler(limits, copy=False)
    # X = np.asanyarray(events)
#     print(X.dtype.char, np.typecodes['AllFloat'], X.dtype.char in np.typecodes['AllFloat'])
#     print("IsFinite?:", np.isfinite(X).all(), np.isfinite(X))
#     print("NaN?:", np.any(np.isnan(X)))
#     print("Inf?:", np.any(np.isinf(X)))
#     print(np.where(np.any(np.isnan(X), axis=1)))
#     nans = np.where(np.any(np.isnan(X), axis=1))
#     print(X[nans])
    scaler.fit(events.values)
    scaled_values = scaler.transform(events.values)
    events.loc[:, :] = scaled_values
    return events, scaler
