'''
Created on Aug 13, 2018

@author: vasot
'''
import ast
import numpy
import logging


logger = logging.getLogger(__name__)


class InputVisitor(ast.NodeVisitor):
    def __init__(self):
        self.container = None

    def visit_Name(self, node):
        if node.id != 'log10':
            self.container = node.id
        return node


def apply_functions(events, inputs):
    new_inputs = []
    for input_param in inputs:
        if input_param not in events.columns:
            logger.debug("{input_param} not found, evaluating...".
                         format(input_param=input_param))
            try:
                events[input_param] = events.eval(input_param,
                                                  parser='python',
                                                  engine='numexpr')
            except ValueError as err:
                if 'log10' in input_param:
                    visitor = InputVisitor()
                    visitor.visit(ast.parse(input_param))
                    param_name = visitor.container
                    events[input_param] = numpy.log10(events[param_name])
                else:
                    continue
        new_inputs.append(input_param)
    return events, new_inputs
