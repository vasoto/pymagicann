'''
Created on Aug 13, 2018

@author: vasoto
'''
from copy import deepcopy
import gc
import logging

from .filter import filter_events
from .normalize import Scaler
from .functions import apply_functions
from pymarsann.utils.mem import get_mem


logger = logging.getLogger('preprocess')


class DataPreprocessor(object):
    def __init__(self,
                 config,  # Configuration
                 inputs,  # Data inputs
                 targets=None,
                 filters=None,
                 drop_duplicates=None,
                 scaler=None,
                 normalize_targets=True,
                 store_events=False,
                 store_index=True):
        self.config = deepcopy(config)
        self.inputs = inputs
        self.targets = targets or []
        self.scaler = scaler
        self.filters = filters or []
        self.index_after_filter = None
        self.drop_duplicates = drop_duplicates
        self.normalize_targets = normalize_targets
        self.store_events = store_events
        self.store_index = store_index
        self.events = None

    def _process_labels(self, events, labels):
        """ Rename dataset labels (inputs, targets, etc.),
        so thay can be used in Pandas
        """
        new_labels = [name.replace('.', '_')
                      for name in labels]
        return apply_functions(events,
                               new_labels)
    
    def _drop_obsolete_data(self, columns, events):
        # Drop obsolite data
        logging.info("Drop obsolete data")
        logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
        cols_to_drop = list(set(events.columns) - set(columns))
        events.drop(cols_to_drop, axis=1, inplace=True)
        logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
        return events

    def _normalize(self, events):
        columns = list(events.columns)

        if not self.normalize_targets:
            columns = list(set(columns) - set(self.targets))

        events = self._drop_obsolete_data(columns, events)
        #events = events[columns].values
        if self.scaler:
            # If scaler is already fit, just transform data
            # Find all columns common for scaler and data
            columns = list(set(events.columns) & set(self.scaler.features))
            # Only normalize the columns available in the scaler
            columns = [column
                       for column in self.scaler.features
                       if column in columns]
            # Get the subset of data to be normalized (columns that are not in that set are removed)
            events = events[columns]
            # DataFrame --> Numpy array
            events = events.values
            # Apply normalization
            events = self.scaler.transform(events,
                                           features=columns)
        else:
            # If the scaler is not loaded, fit the data and then transform
            events = events[columns].values
            self.scaler = Scaler(config=self.config,
                                 features=columns)
            events = self.scaler.fit_transform(events)
        return events, columns

    def _drop_duplicates(self, events):
        if self.drop_duplicates:
            logger.info("Drop duplicated data for {0}".
                        format(self.drop_duplicates))
            orig_size = events.shape[0]
            events.drop_duplicates(subset=self.drop_duplicates,
                                   keep='first',
                                   inplace=True)
            logger.debug("%d non-duplicated events left. %d events are dropped.",
                         events.shape[0],
                         orig_size - events.shape[0])
            logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
        return events

    def _apply_filters(self, events):
        if self.filters:
            logger.debug("Filter data")
            events = filter_events(events, self.filters)
            logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
            if events.shape[0] == 0:
                logger.debug("No data left after filtering!")
                return ([], [])
            if self.store_index:
                # Store new indexes
                self.index_after_filter = events.index
        return events

    def _reorder_data(self, columns, events):
        features_indexes = [columns.index(feature)
                            for feature in self.inputs]
        if len(self.targets):
            # Select features and targets indexes
            columns = self.scaler.features
            targets_indexes = [columns.index(target)
                               for target in self.targets]
            result = (events[:, features_indexes],
                      events[:, targets_indexes])
        else:
            result = (events[:, features_indexes], )

        if self.store_events:
            self.events = events
        del events
        return result

    def fit(self, events):
        # original size
        logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))

        events = self._drop_duplicates(events)
        events = self._apply_filters(events)

        logger.debug("Preprocess inputs")
        events, self.inputs = self._process_labels(events,
                                                   self.inputs)
        logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
        if self.targets:
            logger.debug("Preprocess targets")
            events, self.targets = self._process_labels(events,
                                                        self.targets)
        logger.debug("Memory: %f GB", get_mem()/(1024*1024*1024))
        logger.debug("Normalize data")
        events, columns = self._normalize(events)
        logger.debug("Preprocessing is done")

        result = self._reorder_data(columns, events)
        # Result is a tuple
        total = gc.collect()
        logger.debug("%d objects collected", total)
        return result
