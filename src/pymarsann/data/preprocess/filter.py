'''
Created on Jul 16, 2018

@author: vasot
'''
import re
import logging
import gc

logger = logging.getLogger("filter")


def rename_filter(fltr):
    regex = r"([a-zA-Z_][\w]+)\.([a-zA-Z_][\w]+)"
    subst = r"\1_\2"
    result = re.sub(regex, subst, fltr, 0, re.VERBOSE | re.DOTALL | re.MULTILINE)
    return result


def filter_events(events, filters):
    filters_ = ' & '.join([rename_filter(filt) for filt in filters])
    logger.debug("Replace '.' with '_' in feature names")
    # Replace '.' with '_' in feature names
    events1 = events.rename(lambda name: name.replace('.', '_'),
                            axis='columns')
    # Filter events
    before = events.shape[0]
    logger.debug("Apply filter on all events")
    events1 = events1.query(filters_)
    after = events1.shape[0]
    logger.debug("Removed {0} events out of {before}, {after} left".
                format(before - after,
                       before=before,
                       after=after))
    del events
    # Call garbage collector
    gc.collect()
    return events1
