'''
Created on Aug 19, 2018

@author: vasot
'''
import logging
import warnings
warnings.simplefilter(action='ignore')
from concurrent.futures import ThreadPoolExecutor
import gc

from uproot import iterate
import uproot
import glob
from tqdm import tqdm
import pandas as pd


logger = logging.getLogger(__name__)


class Reader(object):
    """
    Base class for all data readers
    """
    def __init__(self, config):
        self.config = config

    def load(self, files=None, **kwargs):
        """ Load data method - must be overwritten
        """
        raise NotImplementedError("Method 'load' is not implemented.")


class UpRootReader(Reader):
    """
    ROOT files reader based on `uproot` package
    """

    def __init__(self, config, executor=None): #ThreadPoolExecutor()):
        super(UpRootReader, self).__init__(config=config)
        self.executor = executor
    
    def load(self, files, **kwargs):
        assert files is not None, "Please specify files to load"
        data = None
        chunk_count = 0
        # Find the number of events
        num_entries = sum([uproot.numentries(f, treepath=self.config.tree_name)
                           for f in glob.glob(files)])
        
        
        logger.info("Going to read {num_entries} events from {files}, using uproot".
                    format(files=files, num_entries=num_entries))
        chunks = []
        pbar = tqdm(total=num_entries, unit='events', unit_scale=True)
        for start, stop, data_chunk in iterate(path=files,
                                  treepath=self.config.tree_name,
                                  branches=self.config.branches,
                                  executor=self.executor,
                                  outputtype=pd.DataFrame,
                                  reportentries=True):
            pbar.update(stop-start)
            chunks.append(data_chunk)
        pbar.close()
        data = pd.concat(chunks, ignore_index=True)
        
        logger.info("Total {0} events, read in "
                    "{chunk_count} chunks".format(data.shape[0],
                                                  chunk_count=len(chunks)))
        del chunks
        total = gc.collect()
        logger.debug("{0} objects cleaned".format(total))
        # Convert dataset column names from byte-arrays to strings
        data.columns = [col
                        for col in data.columns]
        return data
