'''
Created on Jul 16, 2018

@author: vasot
'''
import uproot
import pandas
import logging
from tqdm import tqdm


logger = logging.getLogger("load")

def load_data(root_file, config, treepath="Events"):
    events = None
    chunk_count = 0
    for i, events_chunk in tqdm(enumerate(uproot.iterate(root_file,
                                                         treepath=treepath,
                                                         branches=config.branches,
                                                         outputtype=pandas.DataFrame))):
#        logger.debug(f"Read chunk {i+1} of {events_chunk.shape[0]} events.")
        if events is None:
            events = events_chunk
        else:
            events = pandas.concat([events, events_chunk],
                                   ignore_index=True)
        chunk_count = i + 1
    logger.info("Total {0} read in {chunk_count} chunks".format(events.shape[0],
                                                                  chunk_count=chunk_count))
    # Convert dataset column names from byte-arrays to strings
    columns = [col.decode() for col in events.columns]
    events.columns = columns
    return events
