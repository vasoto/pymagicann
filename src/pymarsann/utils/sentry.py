import logging

def init_sentry():
    """ Try to initialize connection to sentry
    """
    logger = logging.getLogger('sentry')
    # Add reporting to Sentry
    try:
        import sentry_sdk
        sentry_sdk.init("http://b46a56b1477c405faad9c0844d75e62a@sentry.vastics.pro/2")
        logging.debug('sentry initialized')
    except:
        logging.debug('sentry not available or not initialized')
