'''
Created on Aug 18, 2018

@author: vasot
'''
import logging
import os
try:
    import ROOT as R
except ModuleNotFoundError as _:
    print(
        "ROOT Python module is not installed or cannot be found in the path.")
    exit()

logger = logging.getLogger(__name__)

root_env_set = False


def set_root_env():
    global root_env_set
    if not root_env_set:
        logger.debug("Setting ROOT environment")
        try:
            mars_path = os.environ['MARSSYS']
        except KeyError as _:
            print('Environment variable MARSSYS is not set.')
            exit()
        R.gROOT.Macro(os.path.join(mars_path, "macros", "rootlogon.C"))
        root_env_set = True
    else:
        logger.debug("ROOT environment already set.")
