import os
try:
    import psutil
except Exception:
    import warnings
    warnings.warn("Module psutil is not installed")
    psutil = None


def get_mem():
    if psutil is None:
        return -1
    process = psutil.Process(os.getpid())
    return process.memory_info().rss # in bytes
