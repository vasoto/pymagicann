import re


first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')

def snake_case(name):
    ''' Convert CamelCase text to snake_case
    From https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case 
    '''
    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


def str_to_bool(txt):
    if isinstance(txt, str):
        return txt.lower() in ['true', '1', 't', 'y', 'yes', 'on']
    else:
        return bool(txt)